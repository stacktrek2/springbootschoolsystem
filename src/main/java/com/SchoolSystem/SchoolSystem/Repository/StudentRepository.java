package com.SchoolSystem.SchoolSystem.Repository;

import com.SchoolSystem.SchoolSystem.Entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
}

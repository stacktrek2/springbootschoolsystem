package com.SchoolSystem.SchoolSystem.Controller;

import com.SchoolSystem.SchoolSystem.Entity.Course;
import com.SchoolSystem.SchoolSystem.Service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CourseController {
    @Autowired
    private CourseService courseService;

    @PostMapping("/addCourse")
    public Course addCourse(@RequestBody Course course){
        return courseService.addCourse(course);
    }
    @GetMapping("/getAllCourses")
    public List<Course> getAllCourses(){
        return courseService.getAllCourses();
    }
    @GetMapping("/getCourseById/{id}")
    public Course getCourseById(@PathVariable int id){
        return courseService.getCourseById(id);
    }
    @DeleteMapping("/deleteCourse/{id}")
    public String deleteCourse(@PathVariable int id){
        return courseService.deleteCourse(id);
    }
    @PutMapping("/updateCourse/{id}")
    public Course updateCourse(@PathVariable int id){
        return courseService.updateCourse(id);
    }
}

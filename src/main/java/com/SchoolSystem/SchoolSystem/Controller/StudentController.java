package com.SchoolSystem.SchoolSystem.Controller;

import com.SchoolSystem.SchoolSystem.Entity.Course;
import com.SchoolSystem.SchoolSystem.Entity.Student;
import com.SchoolSystem.SchoolSystem.Service.CourseService;
import com.SchoolSystem.SchoolSystem.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @Autowired
    private CourseService courseService;

    @PostMapping("/addStudent")
    public Student addStudent(@RequestBody Student student){
        return studentService.addStudent(student);
    }
    @GetMapping("/getAllStudents")
    public List<Student> getAllStudents(){
        return studentService.getAllStudents();
    }
    @GetMapping("/getById/{id}")
    public Student getById(@PathVariable int id){
        return studentService.getById(id);
    }
    @DeleteMapping("/deleteById/{id}")
    public String deleteById(@PathVariable int id){
        return studentService.deleteById(id);
    }
    @PutMapping("/updateById/{id}")
    public Student updateById(@RequestBody Student student, @PathVariable int id){
        return studentService.updateById(student, id);
    }
    @PutMapping("/enroll")
    public Student enroll(@RequestParam("studentId") int studentId, @RequestParam("courseId") int courseId) {
        return studentService.enroll(studentId, courseId);
    }

    @PutMapping("/drop")
    public Student drop(@RequestParam("studentId") int studentId, @RequestParam("courseId") int courseId) {
        return studentService.drop(studentId, courseId);
    }

}

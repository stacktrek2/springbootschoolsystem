package com.SchoolSystem.SchoolSystem.StaticData;

import com.SchoolSystem.SchoolSystem.Entity.Student;
import com.SchoolSystem.SchoolSystem.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


@Component
@Profile("staticdata")
public class StaticDataInitializer implements CommandLineRunner {

    private final StudentService studentService;

    @Autowired
    public StaticDataInitializer(StudentService studentService) {
        this.studentService = studentService;
    }

    @Override
    public void run(String... args) {
        // Create and save 10 pre-existing students
        for (int i = 1; i <= 10; i++) {
            Student student = new Student();
            student.setFirstName("Student" + i);
            student.setLastName("Lastname" + i);
            student.setEmail("student" + i + "@example.com");
            studentService.addStudent(student);
        }
    }

}

package com.SchoolSystem.SchoolSystem.Service;

import com.SchoolSystem.SchoolSystem.Entity.Course;
import com.SchoolSystem.SchoolSystem.Repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {
    @Autowired
    private CourseRepository courseRepository;

    public Course addCourse(Course course){
        return courseRepository.save(course);
    }
    public List<Course> getAllCourses(){
        return courseRepository.findAll();
    }
    public Course getCourseById(int id){
        return courseRepository.findById(id).orElse(null);
    }
    public String deleteCourse(int id){
        courseRepository.deleteById(id);
        return "Course Delete with "+id;
    }

    public Course updateCourse(int id){
        Course existingCourse = courseRepository.findById(id).orElse(null);
        existingCourse.setCourseName(existingCourse.getCourseName());
        return courseRepository.save(existingCourse);
    }
//    public Course enroll(Course course){
//
//    }
}

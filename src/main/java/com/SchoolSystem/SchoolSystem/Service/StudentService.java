package com.SchoolSystem.SchoolSystem.Service;

import com.SchoolSystem.SchoolSystem.Entity.Course;
import com.SchoolSystem.SchoolSystem.Entity.Student;
import com.SchoolSystem.SchoolSystem.Repository.CourseRepository;
import com.SchoolSystem.SchoolSystem.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private CourseRepository courseRepository;

    public Student addStudent(Student student){
        return studentRepository.save(student);
    }

    public List<Student> getAllStudents(){
        return studentRepository.findAll();
    }

    public Student getById(int id){
        return studentRepository.findById(id).orElse(null);
    }

    public String deleteById(int id){
        studentRepository.deleteById(id);
        return "Student Removed "+id;
    }

    public Student updateById(Student student, int id){
        Student existingStudent=studentRepository.findById(id).orElse(null);
        existingStudent.getId();
        existingStudent.setFirstName(student.getFirstName());
        existingStudent.setLastName(student.getLastName());
        existingStudent.setEmail(student.getEmail());
        existingStudent.setCourses(student.getCourses());
        return studentRepository.save(existingStudent);
    }
    public Student enroll(int studentId, int courseId) {
        Optional<Student> studentOptional = studentRepository.findById(studentId);
        Optional<Course> courseOptional = courseRepository.findById(courseId);

        if (studentOptional.isPresent() && courseOptional.isPresent()) {
            Student student = studentOptional.get();
            Course course = courseOptional.get();
            student.getCourses().add(course);
            return studentRepository.save(student);
        }
        return null;
    }
    public Student drop(int studentId, int courseId) {
        Optional<Student> studentOptional = studentRepository.findById(studentId);
        Optional<Course> courseOptional = courseRepository.findById(courseId);

        if (studentOptional.isPresent() && courseOptional.isPresent()) {
            Student student = studentOptional.get();
            Course course = courseOptional.get();
            student.getCourses().remove(course);
            return studentRepository.save(student);
        }
        return null;
    }
}
